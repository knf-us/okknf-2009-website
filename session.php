<?php

class Session
{
	var $html, $user;
	var $logged_in = false;
	var $login;

	function Session(&$_html, &$_user)
	{
		$this->html =& $_html;
		$this->user =& $_user;

		session_start();

		if ($_SESSION["uid"] != "") {
			if ($this->user->checkSession($_SESSION["login"], $_SESSION["uid"], 
										 session_id(), $_SERVER["REMOTE_ADDR"]))
			{
				$this->logged_in = true;
				$this->login = $_SESSION["login"];
			}
		}
	}

	function loggedIn()
	{
		return $this->logged_in;
	}

	function getLogin()
	{
		return $this->login;
	}

	function login($login)
	{
		$_SESSION["login"] = htmlspecialchars($login);
		$_SESSION["uid"] = $this->generateRandID();

		$r = $this->user->setFields($login,	array(
							"session" => session_id(),
							"ip" => $_SERVER["REMOTE_ADDR"],
							"uid" => $_SESSION["uid"]));
		return $r;
	}

	function logout()
	{
		if (!$this->loggedIn()) {
			$this->html->errorMsg("Nie jesteś zalogowany(-a) do systemu.");
			return false;
		}

		$this->logged_in = false;

		unset($_SESSION["login"]);
		unset($_SESSION["uid"]);
		return true;
	}

	function generateRandID()
	{
		return md5($this->generateRandStr(16));
	}

	function generateRandStr($length)
	{
		$randstr = "";
		for ($i = 0; $i < $length; $i++){
			$randnum = mt_rand(0,61);
			if($randnum < 10){
				$randstr .= chr($randnum+48);
			} else if ($randnum < 36) {
				$randstr .= chr($randnum+55);
			} else {
				$randstr .= chr($randnum+61);
			}
		}
		return $randstr;
	}
}

?>
