<?php
	include("common.php");

function get_info($login)
{
	global $dbc;

	$trans = array("none" => "", "mgr" => "mgr", "inz" => "inż.", "mgr inz" => "mgr inż.",
				   "dr" => "dr", "dr hab" => "dr hab.", "doc" => "doc.", "prof" => "prof. dr hab.");

	$q = $dbc->query("SELECT us.degree, us.name, us.surname, us.lecture_title, us.lecture, us.desc, un.name AS uni FROM ".
				TBL_USER." us LEFT JOIN unis un ON un.id = us.university WHERE us.login = '$login'");

	$t = $q->fetch(PDO::FETCH_ASSOC);

	if ($t["desc"] != '') {
		$desc = $t["desc"];
	} else {
		$desc = $t["uni"];
	}

	return array($trans[$t["degree"]]." ".$t["name"]." ".$t["surname"], $t["lecture_title"], $t["lecture"], $desc);
}

	include("pages/program-data.php");

	foreach ($program as $day => $prg) {
		foreach ($prg as $key => $val) {
			if ($val != "przerwa" && substr($val, 0, 4) != "food" && substr($val, 0, 4) != "lect" &&
				substr($val, 0, 4) != "desc") {

				if (is_array($val)) {
					list($name1, $title, $abs, $desc) = get_info($val[0]);
					list($name2, $title, $abs, $desc) = get_info($val[1]);
					$name = "$name1, $name2";
					$login = $val[1];
				} else {
					list($name, $title, $abs, $desc) = get_info($val);
					$login = $val;
				}

				print <<<DOC
\\begin{seminar}
	\\semtitle{{$title}}
	\\semauthor{{$name}}
	\\semuni{{$desc}}
	\\begin{semabstract}
$abs
	\\end{semabstract}
\\end{seminar}

DOC;
			}
		}
	}
?>
