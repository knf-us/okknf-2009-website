<?php
	require_once("common.php");
?>
\subsection{Pracownicy naukowi}
\begin{center}
\begin{longtable}{c l l}
	\hline
	\textbf{lp} & \textbf{imię i nazwisko} & \textbf{placówka naukowa} \\ \hline \endhead
<?php


$trans = array("none" => "", "mgr" => "mgr", "inz" => "inż.", "mgr inz" => "mgr inż.",
			   "dr" => "dr", "dr hab" => "dr hab.", "doc" => "doc.", "prof" => "prof. dr hab.");

$q = $dbc->query("SELECT us.degree, us.name, us.surname, ".
				"un.name AS uni, us.desc AS descr FROM ".
				TBL_USER." us LEFT JOIN unis un ON un.id = us.university ".
				"WHERE degree NOT IN ('none','mgr','inz','mgr inz') && active=1 ".
				"ORDER BY surname, name ASC");

$cnt = 1;

while ($t = $q->fetch(PDO::FETCH_ASSOC)) {
	print $cnt." & ".$trans[$t["degree"]]." ".$t["name"]." ".$t["surname"].' & '.$t["descr"].'\\\\'."\n";
	$cnt++;
}

$q->closeCursor();
unset($q);

?>
\hline
\end{longtable}
\end{center}

\subsection{Studenci}
\begin{center}
\begin{longtable}{c l l}
	\hline
	\textbf{lp} & \textbf{imię i nazwisko} & \textbf{uczelnia} \\ \hline \endhead 
<?php

if ($_REQUEST["order"] == "desc") {
	$order = "DESC";
	$iorder = "asc";
} else {
	$order = "ASC";
	$iorder = "desc";
}

if ($_REQUEST["sort"] == "uni") {
	$sort = "un.name";
} else {
	$sort = "surname, name";
}

$q = $dbc->query("SELECT us.degree, us.name, us.surname, ".
				"un.name AS uni, us.desc AS descr FROM ".
				TBL_USER." us LEFT JOIN unis un ON un.id = us.university ".
				"WHERE degree IN ('none','mgr','inz','mgr inz') && active=1 ".
				"ORDER BY $sort $order");

$cnt = 1;

while ($t = $q->fetch(PDO::FETCH_ASSOC)) {
	echo $cnt." & ".$trans[$t["degree"]]." ".$t["name"]." ".$t["surname"]." & ";
	if (!$t["uni"]) {
		echo $t["descr"];
	} else {
		echo $t["uni"];
	}
	echo '\\\\'."\n";
	$cnt++;
}
?>
\hline
\end{longtable}
\end{center}

