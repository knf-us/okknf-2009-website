<?php
	$program = array("Czwartek, 23. kwietnia 2009 r." =>
		array(
			"08:00-20:00" => "desc:Rejestracja uczestników",
			"13:30-15:00" => "food:Obiad",
			"15:00-16:30" => "desc:Czas wolny",
			"16:30-20:00" => "lect:Uroczyste rozpoczęcie konferencji",
			"17:00" => "asleb",
			"18:30-+\\infty" => "desc:Impreza integracyjna",
			),

		"Piątek, 24. kwietnia 2009 r." =>
		array(
			"09:30-10:30" => "food:Śniadanie",
			"10:30-13:20" => "lect:I sesja wykładowa",
			"10:30" => "czarna",
			"11:00" => "spock",
			"11:30" => array("paczuszek", "anita"),
			"12:00" => "przerwa",
			"12:20" => "baskerwil",
			"12:50" => array("miecio", "bandit"),
			"13:20-14:50" => "food:Obiad",
			"14:50-17:40" => "lect:II sesja wykładowa",
			"14:50" => "mkopciuszynski",
			"15:20" => "maciej_czarnacki",
			"15:50" => "przerwa",
			"16:10" => "annabaran",
			"16:40" => "Alchemik",
			"17:10" => "Beyonder",
			"19:00-+\\infty" => "desc:Impreza integracyjna",
		),

		"Sobota, 25. kwietnia 2009 r." =>
		array(
			"09:30-10:30" => "food:Śniadanie",
			"10:30-13:20" => "lect:III sesja wykładowa",
			"10:30" => "jukcyn",
			"11:00" => "iulka",
			"11:30" => "przerwa",
			"11:50" => "Feldmarshall",
			"12:20" => "krupniok",
			"13:20-14:50" => "food:Obiad",
			"14:50-18:00" => "lect:IV sesja wykładowa",
			"14:50" => "Agata",
			"15:20" => "nri",
			"15:50" => "przerwa",
			"16:10" => "ttylec",
			"16:40" => "aptok",
			"17:10" => "wborgiel",
			"19:00-+\\infty" => "desc:Impreza integracyjna"
		),

		"Niedziela, 26. kwietnia 2009 r." =>
		array(
			"09:00-10:00" => "food:Śniadanie",
			"10:00-12:20" => "lect:V sesja wykładowa",
			"10:00" => "mortynbras",
			"10:30" => "julkaj20",
			"11:00" => "przerwa",
			"11:20" => "cornica",
			"11:50" => "WojtekG",
			"12:20-13:20" => "lect:Rozdanie dyplomów",
			"13:20-15:30" => "food:Obiad, zakończenie konferencji"
		)
	);
?>
