<h2>Program naukowy konferencji</h2>

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="schedule">
<?php
function get_info($login)
{
	global $dbc;

	$trans = array("none" => "", "mgr" => "mgr", "inz" => "inż.", "mgr inz" => "mgr inż.",
				   "dr" => "dr", "dr hab" => "dr hab.", "doc" => "doc.", "prof" => "prof. dr hab.");

	$q = $dbc->query("SELECT us.degree, us.name, us.surname, us.lecture_title FROM ".
				TBL_USER." us WHERE us.login = '$login'");

	$t = $q->fetch(PDO::FETCH_ASSOC);

	return array($trans[$t["degree"]]." ".$t["name"]." ".$t["surname"], $t["lecture_title"]);
}

function show_list($list)
{
	foreach ($list as $key => $val) {

		if ($val == "przerwa") {
			print '<tr class="food"><td>'.$key.'</td><td>Przerwa na kawę</td></tr>';
		} else if (substr($val, 0, 4) == "food") {
			print '<tr class="food"><td>'.$key.'</td><td>'.substr($val, 5).'</td></tr>';
		} else if (substr($val, 0, 4) == "lect") {
			print '<tr class="lect"><td>'.$key.'</td><td>'.substr($val, 5).'</td></tr>';
		} else if (substr($val, 0, 4) == "desc") {
			print '<tr><td>'.$key.'</td><td>'.substr($val, 5).'</td></tr>';
		} else {
			if (is_array($val)) {
				list($name1, $title) = get_info($val[0]);
				list($name2, $title) = get_info($val[1]);
				$name = "$name1, $name2";
				$login = $val[1];
			} else {
				list($name, $title) = get_info($val);
				$login = $val;
			}
			print '<tr><td>'.$key.'</td><td><span class="author">'.$name.'</span><br /><a href="abstrakt?login='.$login.'">'.$title.'</a></td></tr>';
		}
	}
}

function show_program($program)
{
	foreach ($program as $day => $prg) {
		echo '<tr><th colspan="2">'.$day.'</th></tr>';
		show_list($prg);
		echo '<tr><td colspan="2">&nbsp;</td></tr>';
	}
}

	include("program-data.php");
	show_program($program);
?>

</table>
