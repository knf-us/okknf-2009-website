<h2>Gdzie jest Brenna?</h2>

<p style="text-align:center;">
<img src="img/map_pl.png" alt="Mapa Polski" /><br />
(na podstawie <a href="http://maps.google.com/maps?f=q&hl=en&q=Gmina+Brenna,+43-438,+Cieszyn+County,+Silesia,+Poland&sll=37.0625,-95.677068&sspn=33.435463,82.705078&ie=UTF8&cd=4&geocode=FQvD9gIdhXogAQ&split=0&ll=51.781436,19.02832&spn=6.866271,19.775391&z=6">>Google Maps</a>)</p>

<h2>Jak dostać się do Brennej?</h2>

Najłatwiej dostać się pociągiem lub autobusem do Skoczowa, a następnie
<a href="http://transkom.pl/php/wyswietlanie/ram4.php?linie[0]=34A1&linie[1]=2A4&linie[2]=37A1&linie[3]=2A3&linie[4]=2A1&linie[5]=2A2&linie[6]=6A2&linie[7]=6A1&linie[8]=11A1&linie[9]=11A2&p_POCZ=SKOCZ%D3W+D.A.&p_KON=BRENNA+O%A6R.ZDR.">autobusem</a>
dojechać do Brennej</a>.

<p>Dogodne połączenie do Skoczowa można znaleźć na stronie <a href="http://rozklad-pkp.pl/">PKP</a>.</p>

<h2>Plan Brennej</h2>

<img src="img/brenna-droga.jpg" alt="Plan Brennej"  />
<p>Na planie zaznaczono drogę z przystanku autobusowego do pensjonatu Biały Orzeł, w którym
odbywa się konferencja.</p>

<ul>
<li><a href="img/brenna-big.jpg">ogólny plan Brennej</a></li>
<li><a href="img/brenna-centrum.jpg">plan centrum Brennej</a></li>
</ul>

