<h2>Organizatorzy</h2>

<img src="img/knf-logo-white_125px.png" alt="logo KNF UŚ" style="width: 125px; float: right; padding-left: 20px;" />
<p>Organizatorem Pikniku Naukowego 2009 jest
<a href="http://knf.us.edu.pl/">Koło Naukowe Fizyków Uniwersytetu Śląskiego</a>.</p>

<p>Komitet Organizacyjny tworzą następujący członkowie KNF UŚ:</p>
<ul>
	<li>Katarzyna Bartuś,</li>
	<li>Natalia Woźnica,</li>
	<li>Wojciech Ciszek -- v-ce prezes ds. finansowych KNF UŚ (2007/2008).</li>
	<li>Michał Januszewski,</li>
	<li>Piotr Szaflik -- prezes KNF UŚ (2008/2009)</li>
</ul>

<h2>Dane kontaktowe</h2>

<p>Wszelkie pytania, zgłoszenia problemów technicznych itp. można kierować
na adres <a href="mailto:konf@knf.us.edu.pl">konf@knf.us.edu.pl</a>.</p>

<p>W razie potrzeby można się z nami również kontaktować telefonicznie dzwoniąc
na numer Koła Naukowego Fizyków UŚ: <b>032 359 16 39</b> lub, w przypadku
pilnych spraw, bezpośrednio z Katarzyną Bartuś pod numerem <b>+48 600 805 265</b>.
</p>


