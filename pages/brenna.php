<h2>Brenna</h2>
<img src="img/LOGO_GMINY_BRENNA.jpg" alt="logo Brennej" style="float: left; padding-right: 10px; width: 150px;" />

<p>Gmina Brenna położona jest w południowej części województwa śląskiego;
w odległości 14 km od Skoczowa, 33 km od Bielska - Białej, 80 km od Katowic, 118 km od Krakowa,
167 km od Zakopanego i 370 km od Warszawy.</p>

<p>Od północnego - zachodu Brenna graniczy ze Skoczowem, od północnego - wschodu z gminami Jasienica, Jaworze i z miastem Bielsko - Biała (grzbietami górskimi: Błotny (Błatnia) - Stołów - Trzy Kopce).</p>
Od wschodu Brenna graniczy ze Szczyrkiem (grzbietem pasma Trzy Kopce - Przełęcz Karkoszczonka - Beskid Węgierski - Przełęcz Salmopolska), na południu graniczy z miastem Wisłą, a na zachodzie z Ustroniem.</p>

<p>Brenna położona jest w dolinie rzeki Brennicy, a w jej skład wchodzą trzy wsie posiadające długą historię: Brenna, Górki Wielkie i Górki Małe.
Pierwsza wzmianka o Brennej w aktach historycznych pochodzi z 1490r., jednakże z jej treści wynika, że wieś ta musiała być
ukształtowana i zamieszkana znacznie wcześniej. Najstarsi osadnicy Brennej to z pewnością ludność polska, rolnicza,
przybyła tu z bardziej północnych terenów Śląska.</p>

<p>Sama nazwa wsi oraz przepływającej przez nią rzeki pochodzą najprawdopodobniej od
staropolsko - słowiańskiego słowa "breń", które w tym języku oznaczało związane z wodą
właściwości środowiska. To słowo wnieśli osadnicy wraz ze swoim językiem, a stwierdziwszy
na zasiedlonym przez siebie obszarze istnienie rozbudowanego systemu wodnego, nadali jego
głównemu ciekowi nazwę "Breń" z końcówka - "ica" typowa dla wielu polskich rzek. Od nazwy
rzeki powstała nazwa wsi Brenna.</p>

<p>Najstarszym z zabytków w Brennej jest kościół w Górkach Wielkich, którego prezbiterium
i węższa część nawy głównej pochodzą z XV w. Do nich w 1662 r, dobudowano szerszą część
wraz z masywną wieżą. Jest to kościół murowany z kamiennymi portalami i rzeźbiarskimi
elementami z XVII - XVIII w. Dzisiejszy wystrój kościoła pochodzi z wieku XIX. Na
zewnętrznej ścianie wieży kościoła w 1981 roku wmurowano tablicę poświęconą Zofii
Kossak. Jej grób można odnaleźć na pobliskim cmentarzu, gdzie spoczywa wraz z ojcem
Tadeuszem i mężem Zygmuntem Szatkowskim. Można również w Górkach zobaczyć ruiny spalonego
dworu oraz domek ogrodnika, w którym Zotia Kossak żyła i tworzyła po 1956 r. W domku tym
mieści się obecnie muzeum jej imienia.</p>

<p>W Brennej znajduje się zabytkowy kościół pod wezwaniem Jana Chrzciciela z końca XVIII w.
Stanowi on bardzo charakterystyczny element architektoniczny wsi. Kościół ten posiada
obecnie wystrój barokowy i klasycystyczny.</p>

<p>Zachowało się tu także kilkanaście zabytkowych chat góralskich, przeważnie z XVIII i XIX w.,
które nie są skupione w jednym miejscu, lecz spotyka się je raczej pojedynczo w różnych częściach
wsi. Jedna z nich, zbudowana jednak już na początku XX w., jest siedzibą Gminnego Ośrodka Kultury.
Znajduje się w niej również izba regionalna ze stałą ekspozycją sprzętów pasterskich, strojów i
innych drobiazgów potrzebnych kiedyś tutejszej ludności do życia wśród gór.</p>

<p>(Tekst na podstawie <a href="http://www.brenna.org.pl">brenna.org.pl</a> i <a href="http://www.brenna.pl/">brenna.pl</a>)</p>

<h2>Więcej informacji</h2>

<ul>
<li><a href="http://www.brenna.org.pl">Oficjalny serwis internetowy gminy Brenna</a></li>
<li><a href="http://www.brenna.pl">Nieoficjalny portal gminy Brenna</a></li>
</ul>


