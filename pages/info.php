<h2>Informacje ogólne</h2>

<ul>
<li>Studentów prosimy o wpłaty:
	<ul>
		<li><b>230 PLN</b> na konto:<br />
		<b>02 1140 2004 0000 3802 4843 2067</b>,<br />
		FHU RASZLO Emanuel Olszar<br />
		43-400 Cieszyn, ul. Bielska 66<br />
		z dopiskiem: "OKKNF"</li>
	</ul>
	<span style="color:red;"><b>UWAGA:</b>
		Faktury będą wystawiane automatycznie na właściciela rachunku, z którego wykonywany będzie przelew
		i nie będzie możliwości ich późniejszej zmiany.  Prosimy o rozważne dokonywanie wpłat.
	</span>
</li>

<li>Jedno koło naukowe może na konferencji reprezentować maksymalnie
7 osób, z czego przynajmniej jedna powinna wygłosić wykład.</li>
<li>Zachęcamy również wszystkich do przygotowywania plakatów na sesję posterową.</li>
<li><b>Ostateczny termin rejestracji mija 13. kwietnia 2009!</b></li>
<li>W przypadku chęci wygłoszenia wykładu należy do <b>13. kwietnia 2009</b> podać jego tytuł oraz abstrakt.</li>
<li>Opłata konferencyjna wynosi <b>230 PLN</b> (dla studentów) i <b>350 PLN</b> (dla pracowników naukowych).</li>
</ul>

<h2>Rejestracja</h2>

<p>Rejestracji na konferencję można dokonać <a href="reg">na tej stronie</a>.
Przy rejestracji należy wypełnić wszystkie wymagane pola formularza rejestracyjnego.
Po zakończeniu procesu rejestracji otrzymasz konto, które pozwoli Ci na określenie
dodatkowych preferencji oraz zmianę niektórych ustawień w dowolnym momencie od
chwili rejestracji do deadline 13. kwietnia.</p>

<p>W przypadku chęci wygłoszenia wykładu lub zaprezentowania posteru naukowego, nie jest
konieczne podawanie jego tytułu i abstraktu w momencie rejestracji. Dane te mogą być
uzupełnione w dowolnym momencie przed 13. kwietnia.</p>

<h2>Wykłady</h2>

<p>Warunkiem koniecznym wygłoszenia wykładu jest podanie jego abstraktu w formularzu
rejestracyjnym.  Abstrakty powinny być napisane w języku polskim lub angielskim, przy czym
język abstraktu musi być taki sam jak język, w którym wygłaszany będzie wykład.  W abstraktach
można używać wyrażeń LaTeX-owych, np. $\int_{-\infty}^{\infty} e^{-x^2} dx = \sqrt{\pi}$
będzie widoczne jako: <img src="img/latex_sample.gif" style="vertical-align:middle;" alt="the gaussian integral" />.
Długość abstraktu nie może przekraczać 250 słów.</p>

<p>Prezentacje nie powinny być dłuższe niż 30 min, z czego 5 min powinno być poświęcone
na pytania od publiczności. Na miejscu będzie dostępny projektor oraz komputer z oprogramowaniem
Acrobat Reader 8.0, MS PowerPoint 2000 oraz OpenOffice 3.0.  W celu uniknięcia problemów
z kompatybilnością, polecamy przygotowanie prezentacji w formacie PDF.</p>

<p><b>UWAGA</b>: Ze względu na ograniczony czas konferencji, w przypadku dużej liczby zgłoszeń
zastrzegamy sobie możliwość wyboru wykładów, które będą prezentowane podczas Pikniku Naukowego 2009.
Decyzja o przyjęciu bądź odrzuceniu wykładu będzie podjęta przez komitet organizacyjny konferencji
do dnia <b>16. kwietnia 2009 r.</b>  Nie przewidujemy podobnych ograniczeń ilościowych w stosunku do
posterów.</p>
