<h2>Program naukowy konferencji</h2>

<p>Poniżej prezentujemy ramowy program konferencji. Dokładny program zostanie
ogłoszony po zakończeniu przyjmowania abstraktów wykładów i posterów.</p>

<p>Przewidujemy <b>jedną sesję interdyscyplinarną</b>, <b>jedną sesję posterową</b>
oraz <b>cztery sesje wykładowe</b> poświęcone zagadnieniom fizyki teoretycznej,
doświadczalnej, stosowanej (biofizyki, fizyki medycznej, geofizyki, etc) oraz
metodom matematycznym i komputerowym fizyki.</p>

<p>Sesja interdyscyplinarna będzie dotyczyć nauk ścisłych innych niż fizyka --
astronomii, matematyki, chemii, informatyki itd.  Mamy nadzieję, że będzie ona okazją do
poznania ciekawych zagadnień i poszerzenia zainteresowań.</p>

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="schedule">
<tr><th colspan="2">Czwartek, 23. kwietnia 2009 r.</th></tr>
<tr><td>08:00-16:00</td><td>Rejestracja uczestników</td></tr>
<tr class="lect"><td>16:30-18:00</td><td>Wykłady inauguracyjne</td></tr>
<tr class="food"><td>18:00-20:00</td><td>Kolacja</td></tr>

<tr><td colspan="2">&nbsp;</td></tr>

<tr><th colspan="2">Piątek, 24. kwietnia 2009 r.</th></tr>
<tr class="food"><td>09:00-10:30</td><td>Śniadanie</td></tr>
<tr class="lect"><td>10:30-13:30</td><td>I sesja wykładowa</td></tr>
<tr class="food"><td>13:30-15:00</td><td>Obiad</td></tr>
<tr class="lect"><td>15:00-18:00</td><td>II sesja wykładowa</td></tr>
<tr class="food"><td>18:00-19:00</td><td>Kolacja</td></tr>
<tr><td>19:00-...</td><td>Impreza integracyjna</td></tr>

<tr><td colspan="2">&nbsp;</td></tr>

<tr><th colspan="2">Sobota, 25. kwietnia 2009 r.</th></tr>
<tr class="food"><td>09:00-10:30</td><td>Śniadanie</td></tr>
<tr class="lect"><td>10:30-13:30</td><td>III sesja wykładowa</td></tr>
<tr class="food"><td>13:30-15:00</td><td>Obiad</td></tr>
<tr class="lect"><td>15:00-18:00</td><td>Sesja interdyscyplinarna i sesja plakatowa</td></tr>
<tr class="food"><td>18:00-19:00</td><td>Kolacja</td></tr>
<tr><td>19:00-...</td><td>Impreza integracyjna.</td></tr>

<tr><td colspan="2">&nbsp;</td></tr>

<tr><th colspan="2">Niedziela, 26. kwietnia 2009 r.</th></tr>
<tr class="food"><td>09:00-10:30</td><td>Śniadanie</td></tr>
<tr class="lect"><td>10:30-12:30</td><td>IV sesja wykładowa</td></tr>
<tr class="food"><td>12:30-14:30</td><td>Obiad, uroczyste zakończenie konferencji</td></tr>
</table>
