<?php
	include("common.php");
?>

\begin{center}
\begin{longtable}{l p{8.5cm} r}
<?php
function get_info($login)
{
	global $dbc;

	$trans = array("none" => "", "mgr" => "mgr", "inz" => "inż.", "mgr inz" => "mgr inż.",
				   "dr" => "dr", "dr hab" => "dr hab.", "doc" => "doc.", "prof" => "prof.");

	$q = $dbc->query("SELECT us.degree, us.name, us.surname, us.lecture_title, us.lecture, us.desc, un.name AS uni, un.short AS short FROM ".
				TBL_USER." us LEFT JOIN unis un ON un.id = us.university WHERE us.login = '$login'");

	$t = $q->fetch(PDO::FETCH_ASSOC);

	if ($t["desc"] != '') {
		$desc = $t["desc"];
	} else {
		$desc = $t["uni"];
	}

	return array($trans[$t["degree"]]." ".$t["name"]." ".$t["surname"], $t["lecture_title"], $t["lecture"], $t["uni"], $t["short"]);
}

	include("pages/program-data.php");
	$shorts = array();

	foreach ($program as $day => $prg) {
		echo '\\multicolumn{3}{c}{\\rowcolor[gray]{.75}\\textbf{'.$day.'}} \\\\'."\n";
		echo '\\hline'."\n";

		foreach ($prg as $key => $val) {
			if ($val == "przerwa") {
				echo '\\rowcolor[gray]{.95} '.$key.' & Przerwa na kawę & \\\\'."\n";
			} else if (substr($val, 0, 4) == "food") {
				echo '\\rowcolor[gray]{.95} '.$key.' & '.substr($val, 5).' & \\\\'."\n";
			} else if (substr($val, 0, 4) == "desc") {
				echo '\\rowcolor[gray]{.98} '.$key.' & '.substr($val, 5).' & \\\\'."\n";
			} else if (substr($val, 0, 4) == "lect") {
				echo '\\hline\\rowcolor[gray]{.9} '.$key.' & \\textbf{'.substr($val, 5).'} & \\\\ \\hline'."\n";
			} else {
				if (is_array($val)) {
					list($name1, $title, $abs, $desc, $short) = get_info($val[0]);
					list($name2, $title, $abs, $desc, $short) = get_info($val[1]);
					$name = "$name1, $name2";
					$login = $val[1];
				} else {
					list($name, $title, $abs, $desc, $short) = get_info($val);
					$login = $val;
				}
				echo $key.' & '.$title.' & \\textit{'.$name.', '.$short.'} \\\\'."\n";
				$shorts["$short"] = "$desc";
			}
		}

		echo '\\hline'."\n\n";
		echo '\\\\[10pt]'."\n";
	}
?>
\end{longtable}
\end{center}

\textbf{Użyte skróty:}\\\\
<?php
	ksort($shorts);
	foreach ($shorts as $short => $full) {
		print "$short - $full\\\\\n";
	}
?>
